﻿using DualDatabasePoc.Infra.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DualDatabasePoc.Api.Command.Controllers
{
    [ApiController]
    [Route("cards-account")]
    public class CardsAccountController : ControllerBase
    {
        private ICardsRepository CardsRepository { get; init; }
        private IUnitOfWorkFactory UnitOfWorkFactory { get; init; }

        public CardsAccountController(
            ICardsRepository cardsRepository,
            IUnitOfWorkFactory unitOfWorkFactory)
        {
            CardsRepository = cardsRepository;
            UnitOfWorkFactory = unitOfWorkFactory;
        }

        [HttpGet("{cardsAccountUid:Guid}")]
        public async Task<IActionResult> CardsAccountExistsAsync(Guid cardsAccountUid)
        {
            var exists = await CardsRepository.CardsAccountExistsAsync(cardsAccountUid);

            return Ok(exists);
        }

        [HttpGet("uow")]
        public IActionResult TestingUow()
        {
            var cardsUow = UnitOfWorkFactory.Create(Database.Cards);
            var cardsIntegrationUow = UnitOfWorkFactory.Create(Database.CardsIntegration);

            cardsUow.BeginTransaction();

            cardsIntegrationUow.BeginTransaction();

            return Ok();
        }
    }
}
