﻿using DualDatabasePoc.Infra.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DualDatabasePoc.Api.Query.Controllers
{
    [ApiController]
    [Route("partner")]
    public class PartnerController : ControllerBase
    {
        private ICardsIntegrationRepository CardsIntegrationRepository { get; init; }

        public PartnerController(ICardsIntegrationRepository cardsIntegrationRepository)
        {
            CardsIntegrationRepository = cardsIntegrationRepository;
        }

        [HttpGet("{partnerAccountId:Int}")]
        public async Task<IActionResult> PartnerAccountExistsAsync(int partnerAccountId)
        {
            var partnerUid = Guid.Parse("66898CB4-E8AA-4DDD-8D9C-C8288E820AD0");
            var exists = await CardsIntegrationRepository.PartnerAccountExistsAsync(partnerUid, partnerAccountId);

            return Ok(exists);
        }
    }
}
