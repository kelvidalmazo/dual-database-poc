﻿using System;
using System.Threading.Tasks;

namespace DualDatabasePoc.Infra.Repository
{
    public interface ICardsRepository
    {
        Task<bool> CardsAccountExistsAsync(Guid cardsAccountUid);
    }
}