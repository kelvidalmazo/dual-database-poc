﻿using System;
using System.Threading.Tasks;

namespace DualDatabasePoc.Infra.Repository
{
    public class CardsRepository : Repository, ICardsRepository
    {
        public CardsRepository(CardsContext context) : base(context) { }

        public Task<bool> CardsAccountExistsAsync(Guid cardsAccountUid)
        {
            var sql = CardsQueries.CardsAccountExists;
            var parameters = new
            {
                CARDS_ACCOUNT_UUID = cardsAccountUid
            };

            return ScalarAsync<bool>(sql, parameters);
        }
    }
}
