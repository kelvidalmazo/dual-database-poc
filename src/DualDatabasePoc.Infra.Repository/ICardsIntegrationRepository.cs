﻿using System;
using System.Threading.Tasks;

namespace DualDatabasePoc.Infra.Repository
{
    public interface ICardsIntegrationRepository
    {
        Task<bool> PartnerAccountExistsAsync(Guid partnerUid, int partnerAccountId);
    }
}