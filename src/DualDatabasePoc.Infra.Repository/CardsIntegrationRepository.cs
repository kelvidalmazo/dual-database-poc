﻿using System;
using System.Threading.Tasks;

namespace DualDatabasePoc.Infra.Repository
{
    public class CardsIntegrationRepository : Repository, ICardsIntegrationRepository
    {
        public CardsIntegrationRepository(CardsIntegrationContext context) : base(context) { }

        public Task<bool> PartnerAccountExistsAsync(Guid partnerUid, int partnerAccountId)
        {
            var sql = CardsIntegrationQueries.PartnerAccountExists;
            var parameters = new
            {
                PARTNER_UUID = partnerUid,
                PARTNER_ACCOUNT_ID = partnerAccountId
            };

            return ScalarAsync<bool>(sql, parameters);
        }
    }
}
