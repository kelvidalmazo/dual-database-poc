﻿using System.Data;

namespace DualDatabasePoc.Infra.Repository
{
    public abstract class DatabaseContext
    {
        public IDbConnection Connection { get; init; }
        public IDbTransaction Transaction { get; set; }

        protected DatabaseContext(string connectionString)
            => Connection = ConnectionFactory.BeginConnection(connectionString);
    }
}
