﻿namespace DualDatabasePoc.Infra.Repository
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create(Database database);
    }
}
