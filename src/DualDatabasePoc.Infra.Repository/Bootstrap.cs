﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DualDatabasePoc.Infra.Repository
{
    public static class Bootstrap
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddContexts(configuration)
                .AddScoped<IUnitOfWorkFactory, UnitOfWorkFactory>()
                .AddScoped<ICardsRepository, CardsRepository>()
                .AddScoped<ICardsIntegrationRepository, CardsIntegrationRepository>();

            return services;
        }

        private static IServiceCollection AddContexts(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddScoped<CardsContext>(fac => new(configuration.GetConnectionString("Cards")))
                .AddScoped<CardsIntegrationContext>(fac => new(configuration.GetConnectionString("CardsIntegration")));

            return services;
        }
    }
}
