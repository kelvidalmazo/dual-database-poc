﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace DualDatabasePoc.Infra.Repository
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private IServiceProvider Provider { get; init; }

        public UnitOfWorkFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IUnitOfWork Create(Database database)
            => new UnitOfWork(GetContext(database));

        private DatabaseContext GetContext(Database database)
        {
            var type = database switch
            {
                Database.Cards => typeof(CardsContext),
                Database.CardsIntegration => typeof(CardsIntegrationContext),
                _ => throw new NotImplementedException(),
            };

            return (DatabaseContext)Provider.GetRequiredService(type);
        }
    }
}
