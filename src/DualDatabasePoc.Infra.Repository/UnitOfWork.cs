﻿using System;

namespace DualDatabasePoc.Infra.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool Disposed { get; set; } = false;
        private DatabaseContext Context { get; set; }

        public UnitOfWork(DatabaseContext context)
        {
            Context = context;
        }

        public void Commit()
        {
            try
            {
                if (Context.Transaction is not null)
                {
                    Context.Transaction.Commit();
                    ResetTransaction();
                }
            }
            catch
            {
                Rollback();
            }
        }

        public void BeginTransaction()
        {
            if (Context.Transaction is null)
                Context.Transaction = Context.Connection.BeginTransaction();
        }

        public void Rollback()
        {
            if (Context.Transaction is not null)
            {
                Context.Transaction.Rollback();
                ResetTransaction();
            }
        }

        private void ResetTransaction()
            => Context.Transaction = null;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (Disposed) return;

            if (isDisposing)
            {
                Context.Connection?.Dispose();
                Context.Transaction?.Dispose();
                Context = null;
            }

            Disposed = true;
        }
    }
}
