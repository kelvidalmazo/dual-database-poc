﻿namespace DualDatabasePoc.Infra.Repository
{
    public static class CardsQueries
    {
        public const string CardsAccountExists =
            @"SELECT COUNT(CA.CARDS_ACCOUNT_UUID)
              FROM DBO.CARDS_ACCOUNT CA
              WHERE CA.CARDS_ACCOUNT_UUID = @CARDS_ACCOUNT_UUID;";
    }
}
