﻿using System.Data;
using System.Data.SqlClient;

namespace DualDatabasePoc.Infra.Repository
{
    public static class ConnectionFactory
    {
        public static IDbConnection BeginConnection(string connectionString)
        {
            var connection = new SqlConnection(connectionString);
            connection.Open();

            return connection;
        }
    }
}
