﻿using Dapper;
using System.Threading.Tasks;

namespace DualDatabasePoc.Infra.Repository
{
    public abstract class Repository
    {
        private DatabaseContext Context { get; init; }

        protected Repository(DatabaseContext context)
        {
            Context = context;
        }

        protected Task<T> ScalarAsync<T>(string sql, object parameters = null)
        {
            CommandDefinition command = new(sql, parameters, Context.Transaction);

            return Context.Connection.ExecuteScalarAsync<T>(command);
        }
    }
}
