﻿namespace DualDatabasePoc.Infra.Repository
{
    public static class CardsIntegrationQueries
    {
        public const string PartnerAccountExists =
            @"SELECT COUNT(CPAD.CARDS_ACCOUNT_UUID)
              FROM DBO.CARDS_PARTNER_ACCOUNT_DETAIL CPAD
              WHERE 
	            CPAD.PARTNER_UUID = @PARTNER_UUID
	            AND CPAD.PARTNER_ACCOUNT_ID = @PARTNER_ACCOUNT_ID;";
    }
}
